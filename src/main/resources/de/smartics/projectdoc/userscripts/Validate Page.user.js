/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// ==UserScript==
// @name         Validate Page
// @namespace    http://smartics.de/go/projectdoc
// @version      0.1
// @description  Some basic link checks in the body of a page in view mode.
// @author       smartics
// @match        https://*/confluence/*
// @grant        none
// ==/UserScript==

(function () {
  'use strict';
  AJS.toInit(function () {
    const logToConsole = true;
    const invalidCss = {
      "background-color": "darkred",
      "color": "white",
      "font-weight": "bold"
    };
    const reportCors = false;
    const corsCss = {
      "background-color": "orange",
      "color": "black",
      "font-weight": "bold"
    };

    const baseUrl = AJS.Meta.get('base-url');
    const delimIndex = baseUrl.lastIndexOf("/");
    const serverUrl = delimIndex > 0 ? baseUrl.substring(0, delimIndex) : baseUrl;

    if (logToConsole) AJS.log("[validate-page] Start validating page ...");

    const $content = AJS.$("#content");
    if (!$content.length || !$content.hasClass("page view")) {
      if (logToConsole) AJS.log("[validate-page] Not visiting a page. Quitting.");
      return;
    }

    const checkUrl = function (counter, linkTracker, url, $linkElement, label) {
      AJS.$.ajax({
        beforeSend: function (jqXHR, settings) {
          settings.url = url;
        },
        type: "HEAD",
        async: true,
        redirect: 'follow',
        url: url,
      }).success(function () {
        if (logToConsole) AJS.log(AJS.format("[validate-page] Valid URL: {0}", url));
        linkTracker.linksChecked++;
      }).error(function (jqXHR, textStatus) {
        const httpStatus = jqXHR.status;
        if (logToConsole) AJS.log(AJS.format("[validate-page] Invalid URL: {0} ({1}:{2})", url, textStatus, httpStatus));

        if (httpStatus === 0) {
          if (logToConsole) AJS.log("[validate-page] CORS issue detected!");
        }
        if(httpStatus !== 0 || reportCors) {
          const ref = markElement(counter, $linkElement, httpStatus === 0 ? corsCss : invalidCss);
          const invalidUrl = {
            url: url,
            label: label,
            status: httpStatus,
            ref: ref
          };
          linkTracker.invalidUrls.push(invalidUrl);
          renderReport('Interims Page Validation Report');
        }
        linkTracker.linksChecked++;
      });
    };

    const findMessageContainer = function () {
      let $messageContainer = AJS.$("#messageContainer");
      if ($messageContainer.length) {
        const $li = AJS.$("<li></li>");
        $messageContainer.append($li);
        return $li;
      }

      $messageContainer = AJS.$("#action-messages");
      if ($messageContainer.length) {
        return $messageContainer;
      }

      $messageContainer = AJS.$("#full-height-container");
      return $messageContainer;
    };

    const markElement = function (counter, $element, css) {
      $element.css(css);

      let ref = $element.attr("id");
      if (!ref) {
        ref = "validate-page-" + counter;
        $element.attr("id", ref);
      }
      return ref;
    };

    const anchorsInContent = AJS.$("#main-content").find("a");
    const linkTracker = {
      linksProcessed: 0,
      processedUrls: [],
      invalidUrls: [],
      linksChecked: 0
    }
    AJS.$.each(anchorsInContent, function () {
      linkTracker.linksProcessed++;
      const $currentAnchor = AJS.$(this);
      if (!$currentAnchor.length) AJS.log("[validate-page] Cannot access current anchor!!!!!!!!");
      const url = AJS.$.trim($currentAnchor.attr("href"));
      const label = $currentAnchor.text();

      linkTracker.processedUrls.push(url);

      if ($currentAnchor.hasClass("createlink")) {
        const invalidUrl = {
          url: url,
          label: label,
          status: "create link"
        };
        linkTracker.invalidUrls.push(invalidUrl);
        linkTracker.linksChecked++;
      } else {
        if (url) {
          if (logToConsole) AJS.log("[validate-page] Checking " + url + " ...");

          if (url.startsWith("#")) {
            if (url.length > 1) {
              const $element = AJS.$(url);
              if (!$element.length) {
                const invalidUrl = {
                  url: url,
                  label: label,
                  status: "invalid-in-page-reference"
                };
                linkTracker.invalidUrls.push(invalidUrl);
                markElement(linkCounter, $currentAnchor);
              }
            }
            linkTracker.linksChecked++;
          } else {
            let requestUrl;
            if (url.startsWith("/")) {
              requestUrl = serverUrl + url;
              checkUrl(linkTracker.linksProcessed, linkTracker, requestUrl, $currentAnchor, label);
            } else {
              requestUrl = url;
              checkUrl(linkTracker.linksProcessed, linkTracker, requestUrl, $currentAnchor, label);
              // if (logToConsole) AJS.log("Skip accessing remote URLs (CORS) ...");
            }
          }
        } else {
          linkTracker.linksChecked++;
        }
      }
    });

    const renderReport = function (title) {
      if (linkTracker.invalidUrls.length > 0) {
        if (logToConsole) AJS.log(AJS.format("[validate-page] Page found {0} issues according to configured checks!"), linkTracker.invalidUrls.length);
        const $messageContainer = findMessageContainer();
        if ($messageContainer.length) {
          const $message = AJS.$('<div id="userscript-page-report" class="aui-message aui-message-error">\n' +
            '<p class="title">\n' +
            '  <strong>' + title + '</strong>\n' +
            '</p>\n' +
            '<p>The following issues have been encountered.</p>\n' +
            '<h3>Invalid URLs</h3>\n' +
            '<p>The page contains ' + linkTracker.invalidUrls.length + ' URLs with issues.</p>\n' +
            '<ol id="invalid-urls"></ol>\n' +
            '</div>');

          const $ol = AJS.$($message).find("#invalid-urls");
          if ($ol.length) {
            AJS.$.each(linkTracker.invalidUrls, function (index, invalidUrl) {
              if (logToConsole) AJS.log("[validate-page] Processing invalid URL: " + JSON.stringify(invalidUrl));
              const $li = AJS.$('<li></li>');
              const $reportLine = AJS.$("<div>" + invalidUrl.url + " (<a href='#" + invalidUrl.ref + "'>" + invalidUrl.label + "</a>): " + invalidUrl.status + "</div>");
              $li.append($reportLine);
              $ol.append($li);
            });
            const $oldMessage = $messageContainer.find("#userscript-page-report");
            if ($oldMessage) {
              $oldMessage.remove();
            }
            $messageContainer.append($message);
          } else {
            AJS.log(AJS.format("[validate-page] Failed to locate own list by ID #invalid-urls! Skipping report ..."));
          }
        } else {
          AJS.log(AJS.format("[validate-page] Failed to locate message container on page with ID #messageContainer! Skipping report ..."));
        }
      } else {
        if (logToConsole) AJS.log("[validate-page] Page valid according to configured checks!");
      }
    };

    let intervalsCount = 0;
    let intervalId = setInterval(
      function () {
        intervalsCount++;
        if (logToConsole) AJS.log(intervalsCount + ". interval: Status of checks: checked:" + linkTracker.linksChecked + " vs. processed:" + linkTracker.linksProcessed);
        if (linkTracker.linksChecked >= linkTracker.linksProcessed || intervalsCount > 30) {
          if (logToConsole) AJS.log("Finished with checks, rendering report ...");
          if (linkTracker.linksChecked < linkTracker.linksProcessed) {
            let consoleReport = "List of processed links (checked:" + linkTracker.linksChecked + " vs. processed:" + linkTracker.linksProcessed + ")\n";
            AJS.$.each(linkTracker.processedUrls, function (index, processedUrl) {
              consoleReport += "  " + index + ". " + processedUrl + "\n";
            });
            AJS.log(consoleReport);
          }

          renderReport('Final Page Validation Report');
          clearInterval(intervalId);
          if (logToConsole) AJS.log("Report rendered.");
        }
      }, 1500);
  });
})();
