/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

if (AJS) {
  AJS.toInit(function () {
    function hideElements($element) {
      if ($element) {
        $element.css("display", "none");
      }
    }

    function hideSpaceBlueprints() {
      const $templateElements = AJS.$('#create-dialog .templates .template[data-blueprint-module-complete-key*="projectdoc"][data-create-result="space"]');
      hideElements($templateElements);
    }
    function hidePageBlueprints() {
      const $templateElements = AJS.$('#create-dialog .templates .template[data-blueprint-module-complete-key*="projectdoc"]:not([data-create-result="space"])');
      hideElements($templateElements);
    }
    function hideMacros() {
      const $macroElements = AJS.$('#select-macro-page .dialog-page-body .macro-list-item[id*="projectdoc"]');
      hideElements($macroElements);
    }
    function hideAutocompleteMacros() {
      const $macroElements = AJS.$('li>a[class*="autocomplete-macro-projectdoc"]');
      if ($macroElements) {
        $macroElements.parent().css("display", "none");
      }
    }

    hideSpaceBlueprints();
    hidePageBlueprints();
    hideMacros();
    hideAutocompleteMacros();
  });
}
