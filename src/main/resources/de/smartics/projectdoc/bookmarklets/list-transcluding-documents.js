/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const pageId = AJS.Meta.get('page-id');
  const locale = AJS.Meta.get('user-locale');
  const baseUrl = AJS.Meta.get('base-url');

  function createPage(i18n, currentDocument, documents) {
    const htmlTitle = "Transcluding Documents for " + currentDocument[i18n.name];
    let html = "<html lang='" + locale + "'><head><title>" + htmlTitle + "</title><style>" +
      " body {margin: 1rem !important;}" +
      " .table-sm td, .table-sm th {padding: .1rem !important;}" +
      " .table td, .table th { font-size: .8rem !important;}" +
      "</style>" +
      "<link rel=\"stylesheet\"" +
      " href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\"" +
      " integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\"" +
      " crossorigin=\"anonymous\">" +
      "</head><body><h3>" + htmlTitle + "</h3>";
    html += "<table class=\"table table-sm table-bordered table-striped\">";

    const hitCount = documents.document.length;
    const tinyUrlNamePlain = i18n["tinyUrl"] + '\u00a7';
    html += "<p>Content of document '<a href='" + currentDocument[tinyUrlNamePlain] + "'>" + currentDocument[i18n.name] + "</a>' is transcluded by ";
    if (hitCount === 0) {
      html += "no document.</p>";
    } else if (hitCount === 1) {
      html += "one document.</p>";
    } else {
      html += hitCount + " documents.</p>";
    }

    $.each(documents.document, function (index, doc) {
        const current = {};
        $.each(doc.property, function (i, property) {
          current[property.name] = property.value;
        });
        const name = current[i18n.name];
        const shortDescription = current[i18n.shortDescription];
        const url = current[tinyUrlNamePlain];
        html = html + "<tr><th><a href='" + url + "'>" + name + "</a></th><td>" + shortDescription + "</td></tr>";
      }
    );

    html += "</table></body></html>";
    return html;
  }

  const i18n = PDBMLS.fetchI18n(baseUrl, ["title", "spaceKey", "name", "shortDescription", "tinyUrl"]);
  const tinyUrlNamePlain = i18n["tinyUrl"] + '\u00a7';
  const currentDocument = PDBMLS.fetchDocument(baseUrl, pageId, [i18n.spaceKey, i18n.title, i18n.name, tinyUrlNamePlain]);

  if (currentDocument) {
    const spaceKey = currentDocument[i18n.spaceKey];
    const title = currentDocument[i18n.title];
    const pageReference = spaceKey + "." + title;
    const where = "$<TranscludedDocumentTitles>=[" + pageReference + "]";

    const documents = PDBMLS.fetchDocuments(baseUrl, [i18n.name, i18n.shortDescription, tinyUrlNamePlain], where);
    let html = createPage(i18n, currentDocument, documents);

    const container = window.open('', '', 'width=600,height=800,location=no,toolbar=0');
    container.document.body.innerHTML = html;
  }
});
