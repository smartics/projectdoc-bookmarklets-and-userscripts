/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

const baseUrl = "http://localhost:1990/confluence";

function extractMetadataFromDocument(metadataDictionary) {
  const metadataMap = new Map();
  metadataMap.set("source", window.location.href);

  const metaElements = document.getElementsByTagName("meta");
  for (let i = 0; i < metaElements.length; i++) {
    const elem = metaElements[i];
    let elementName = elem.getAttribute('name');
    if (elementName === null) {
      elementName = elem.getAttribute('property');
    }
    if (elementName !== null) {
      const metadataName = metadataDictionary[elementName];
      if (metadataName !== undefined) {
        const value = elem.getAttribute('content');
        if (value) {
          metadataMap.set(metadataName, value.trim());
        }
      }
    }
  }

  if (!metadataMap.get("name")) {
    const titles = document.getElementsByTagName("title");
    if (titles.length > 0) {
      const value = titles.item(0).textContent;
      if (value) {
        metadataMap.set("name", value.trim());
      }
    }
  }

  const selectedText = getSelectedText();
  if (selectedText) {
    metadataMap.set("description", selectedText.trim());
  }
  return metadataMap;
}

function getSelectedText() {
  let selectedText;
  if (window.getSelection) {
    selectedText = window.getSelection().toString();
  } else if (document.selection && document.selection.type !== "Control") {
    selectedText = document.selection.createRange().text;
  }
  return selectedText;
}

function renderWindow(metadataMap) {
  const screenWidth = screen.width, screenHeight = screen.height,
    popupWidth = 640, popupHeight = 580;
  let popupLeft = 0, popupTop = 0;
  if (screenWidth > popupWidth) {
    popupLeft = Math.round((screenWidth / 2) - (popupWidth / 2));
  }
  if (screenHeight > popupHeight) {
    popupTop = Math.round((screenHeight / 2) - (popupHeight / 2));
  }

  let parameters = "";
  for (const [key, value] of metadataMap) {
// console.log("  " + key + " = " + value);
    if(parameters.length !== 0) {
      parameters += "&";
    }
    parameters += encodeURIComponent(key) + "=" + encodeURIComponent(value);
  }

  const actionUrl = baseUrl + "/plugins/projectdoc-bookmarklets-extension/bookmarklet.action";
  window.open(actionUrl + '?' + parameters,
    '', 'left=' + popupLeft + ',top=' + popupTop + ',width=' + popupWidth + ',height=' + popupHeight + ',personalbar=0,toolbar=0,scrollbars=1,resizable=1');
}

const metadataDictionary = {
  "DC.title": "name",
  "og:image": "imageUrl",
  "description": "shortDescription",
  "twitter:site": "twitter"
};
const metadataMap = extractMetadataFromDocument(metadataDictionary);
renderWindow(metadataMap);
