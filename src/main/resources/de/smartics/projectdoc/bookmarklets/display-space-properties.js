/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const spaceKey = AJS.Meta.get('space-key');
const locale = AJS.Meta.get('user-locale');
const baseUrl = AJS.Meta.get('base-url');

var result = $.ajax({
  url: baseUrl + "/rest/projectdoc/1/space/" + spaceKey,
  async: false,
  dataType: 'json'
}).responseText;

console.log("Result: " + result);

const htmlTitle = "Space Properties for " + spaceKey;
var html = "<html lang='" + locale + "'><head><title>" + htmlTitle + "</title><style>" +
  " .table-sm td, .table-sm th {padding: .1rem !important;}" +
  " .table td, .table th { font-size: 10px !important;}" +
  "</style>" +
  "<link rel=\"stylesheet\"" +
  " href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\"" +
  " integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\"" +
  " crossorigin=\"anonymous\">" +
  "</head><body><h6>" + htmlTitle + "</h6>";
html += "<table class=\"table table-sm table-bordered table-striped\">";
$.each($.parseJSON(result).property,
  function (index, obj) {
    html = html + "<tr><td>" + obj.source + "</td><th>" + obj.name + "</th><td>" + obj.value + "</td></tr>"
  }
);
html += "</table></body></html>";

const x = window.open('', '', 'width=600,height=800,location=no,toolbar=0');
x.document.body.innerHTML = html;
