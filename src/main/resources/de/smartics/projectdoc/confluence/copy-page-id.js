/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;
  if (logToConsole) AJS.log("[copy-page-id] Registering action 'Copy Page ID to Clipboard' to keydown 'p'...");

  AJS.whenIType('p').execute(function () {
    if (logToConsole) AJS.log("[copy-page-id] Start copying to page ID " + AJS.Meta.get('page-id') + " to clipboard ...");

    const tmp = document.createElement('textarea');
    tmp.value = AJS.Meta.get('page-id');
    tmp.style = {position: 'absolute', left: '-9999px'};
    tmp.setAttribute('readonly', '');
    document.body.appendChild(tmp);
    tmp.select();
    document.execCommand('copy');
    document.body.removeChild(tmp);
  });
});
