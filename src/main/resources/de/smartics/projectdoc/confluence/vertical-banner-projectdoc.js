/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const API_URL = "/rest/projectdoc/1";
  const logToConsole = false;
  const bannerWidthWithLabel = 32;
  const bannerWidthWithoutLabel = 6;

  if (logToConsole) {
    AJS.log("[vertical-banner] Locating ...");
  }

  const $main = AJS.$("#main");
  if ($main.length) {
    const fetchSpaceCategory = function () {
      const baseUrl = AJS.Meta.get('base-url');
      const spaceKey = AJS.Meta.get('space-key');

      const i18n = PDBMLS.fetchI18n(baseUrl, ["subject"]);
      const spaceSubjectPropertyName = i18n["subject"];
      const url = baseUrl + API_URL + "/space/" + encodeURIComponent(spaceKey) + "/property/" + encodeURIComponent(spaceSubjectPropertyName) + ".json";
      let spaceCategory = AJS.Meta.get('space-name');
      AJS.$.ajax({
        url: url,
        async: false,
        contentType: 'application/json'
      }).success(function (data) {
        AJS.log("[vertical-banner] Data: " + JSON.stringify(data));
        spaceCategory = data["value"];
      }).error(function (jqXHR, textStatus) {
        AJS.log("[vertical-banner] Error fetching space category: " + textStatus);
      });
      AJS.log("[vertical-banner] Returning: " + spaceCategory);
      return spaceCategory;
    }

    const encodeCss = function (prefix, input) {
      return prefix + "-" + input.replace(/[^a-z0-9_-]/g, function (string) {
        var char = string.charCodeAt(0);
        if (char === 32) {
          return '-';
        }
        if (char >= 65 && char <= 90) {
          return string.toLowerCase();
        } else {
          return '_';
        }
      });
    };

    const paddingLeft = parseInt($main.css("padding-left").replace('px', ''));
    const minWidth = paddingLeft > bannerWidthWithLabel ? bannerWidthWithLabel : bannerWidthWithoutLabel;
    const $sidebarContainer = AJS.$("#sidebar-container");
    if ($sidebarContainer.length) {
      const spaceCategory = fetchSpaceCategory();
      if (spaceCategory) {
        const sizeCss = minWidth == bannerWidthWithLabel ? "large" : "small";
        const $banner = AJS.$("<div id=\"userscripts-vertical-banner\"></div>\n");
        const spaceCategoryCss = encodeCss("projectdoc", spaceCategory);
        $banner.addClass("projectdoc-" + sizeCss);
        $banner.addClass(spaceCategoryCss);

        if (minWidth == bannerWidthWithLabel) {
          const $label = AJS.$("<div id=\"userscripts-vertical-banner-label\"></div>\n");
          $label.addClass(spaceCategoryCss);
          $label.text(spaceCategory);
          $banner.append($label);
        }
        $sidebarContainer.append($banner);
      }
    }
  }
})
;
