/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const bannerWidthWithLabel = 32;
  const bannerWidthWithoutLabel = 6;

  const $main = AJS.$("#main");
  if ($main.length) {
    const fetchLabel = function () {
      return AJS.Meta.get('space-name');
    }
    const fetchColors = function () {
      return ["blueviolet", "white"];
    }

    const paddingLeft = parseInt($main.css("padding-left").replace('px', ''));
    const minWidth = paddingLeft > bannerWidthWithLabel ? bannerWidthWithLabel : bannerWidthWithoutLabel;
    const $sidebarContainer = AJS.$("#sidebar-container");
    if ($sidebarContainer.length) {
      const $banner = AJS.$("<div id=\"userscripts-vertical-banner\"></div>\n");

      const colors = fetchColors();

      $banner.css({
        "background-color": colors[0],
        "visibility": "visible",
        "z-index": "100",
        "min-width": minWidth + "px",
        "max-width": minWidth + "px",
        "min-height": "-webkit-fill-available",
        "margin-left": "0",
        "margin-top": "0",
        "position": "absolute",
        "display": "block",
        "top": "0",
        "left": "0"
      });

      if (minWidth == bannerWidthWithLabel) {
        const $label = AJS.$("<div id=\"userscripts-vertical-banner-label\"></div>\n");
        const label = fetchLabel();
        $label.text(label);
        $label.css({
          "font": "bold 12px Sans-Serif",
          "color": colors[1],
          "letter-spacing": "2px",
          "text-transform": "uppercase",
          "line-height": "24px",
          "transform-origin": "0 0",
          "transform": "rotate(-90deg)",
          "width": "30em",
          "height": "100%",
          "margin-left": "5px",
          "margin-top": "400px",
          "z-index": "200px",
          "position": "sticky"
        });
        $banner.append($label);
      }

      $sidebarContainer.append($banner);
    }
  }
});
