/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;

  if (logToConsole) {
    AJS.log("[shortcuts-for-focus] Installing focus shortcut ...");
  }

  const setFocus = function (selector) {
    if (logToConsole) {
      AJS.log("[shortcuts-for-focus] Trying to set focus to '" + selector + "' ...");
    }
    const $element = AJS.$(selector);
    if ($element.length) {
      $element.focus();
      if (logToConsole) {
        AJS.log("[shortcuts-for-focus] Set focus to '" + selector + "' successfully!");
      }
      return true;
    }
    return false;
  }

  USERSCRIPT4C_SYNC.syncWithElement('#create-dialog', function ($listener) {
    $listener.bind('keydown', 'f', function (e) {
      e.preventDefault();
      setFocus('#createDialogFilter');
    });
  }, document);
});
