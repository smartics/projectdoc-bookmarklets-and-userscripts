/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = true;

  if (logToConsole) {
    AJS.log("[NOUI import] Importing userscripts ... !");
  }

  const userscripts = JSON.parse("[\n" +
    "  {\n" +
    "    \"namespace\": \"de.smartics.confluence\",\n" +
    "    \"name\": \"count-selection\",\n" +
    "    \"version\": \"0.1\",\n" +
    "    \"description\": \"Count Selection\",\n" +
    "    \"activation\": {\n" +
    "      \"label\": [\n" +
    "        \"count\"\n" +
    "      ]\n" +
    "    },\n" +
    "    \"script\": \"https://www.example.com/script-repo/count-section.js?api=v2\"\n" +
    "  }" +
    "]");

  const baseUrl = AJS.Meta.get('base-url');
  const serviceUrl = baseUrl + "/rest/userscripts-for-confluence/1/userscripts";
  const method = "POST";
  AJS.$.each(userscripts,
    function (index, userscript) {
      if (logToConsole) AJS.log("[NOUI import] " + method + " to " + serviceUrl + "\n" + JSON.stringify(userscript));
      AJS.$.ajax(serviceUrl, {
        async: true,
        type: method,
        data: JSON.stringify(userscript),
        dataType: 'application/json',
        contentType: 'application/json'
      }).success(function (data) {
        if (logToConsole) AJS.log("[NOUI import] Successfully submitted to " + serviceUrl + "\n" + JSON.stringify(userscript));
      }).error(function (jqXHR, textStatus) {
        if (jqXHR.status >= 200 && jqXHR.status < 300) {
          if (logToConsole) AJS.log("[NOUI import] Successfully submitted to " + serviceUrl + "\n" + JSON.stringify(jqXHR));
        } else {
          if (logToConsole) AJS.log("[NOUI import] Error submitting to " + serviceUrl + " " + jqXHR.status + " (" + textStatus + ")\n" + JSON.stringify(userscript));
        }
      });
    }
  );
});
