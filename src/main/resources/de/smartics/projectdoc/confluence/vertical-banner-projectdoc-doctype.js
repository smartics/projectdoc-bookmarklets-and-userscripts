/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const API_URL = "/rest/projectdoc/1";
  const logToConsole = false;
  const bannerWidthWithLabel = 32;
  const bannerWidthWithoutLabel = 6;

  if (logToConsole) {
    AJS.log("[vertical-banner] Locating ...");
  }

  const $main = AJS.$("#main");

  if ($main.length) {
    const fetchProperties = function (doctype) {
      const baseUrl = AJS.Meta.get('base-url');
      const spaceKey = AJS.Meta.get('space-key');

      const doctypeKey = "projectdoc." + doctype + ".template.title";
      const i18n = PDBMLS.fetchI18n(baseUrl, ["subject", doctypeKey]);
      const spaceSubjectPropertyName = i18n["subject"];
      const url = baseUrl + API_URL + "/space/" + encodeURIComponent(spaceKey) + ".json?property-filter=" + encodeURIComponent(spaceSubjectPropertyName + "|projectdoc-banner-doctypes")
      const defaultData = {};
      if (doctype) {
        const doctypeName = i18n[doctypeKey];
        if(doctypeName !== doctypeKey) {
          defaultData['doctypeName'] = doctypeName;
        } else {
          defaultData['doctypeName'] = doctype;
        }
      }
      AJS.$.ajax({
        url: url,
        async: false,
        contentType: 'application/json'
      }).success(function (data) {
        const properties = data["property"];
        if (properties.length) {
          for (let i = properties.length - 1; i >= 0; i--) {
            const property = properties[i];
            const value = property['value'];
            const name = property['name'];
            if (spaceSubjectPropertyName === name) {
              defaultData["subject"] = value;
            } else {
              defaultData[name] = value;
            }
          }
        } else {
          defaultData["subject"] = AJS.Meta.get('space-name');
        }
      }).error(function (jqXHR, textStatus) {
        AJS.log("[vertical-banner] Error fetching space category: " + textStatus);
        defaultData["subject"] = AJS.Meta.get('space-name');
      });
      return defaultData;
    }

    const encodeCss = function (prefix, input) {
      return prefix + "-" + input.replace(/[^a-z0-9_-]/g, function (string) {
        var char = string.charCodeAt(0);
        if (char === 32) {
          return '-';
        }
        if (char >= 65 && char <= 90) {
          return string.toLowerCase();
        } else {
          return '_';
        }
      });
    };
    const fetchBannerRequests = function (properties, doctype) {
      const requests = {};
      const doctypesString = properties['projectdoc-banner-doctypes'];
      if (doctypesString) {
        var doctypes = doctypesString.split(/,\s?/);
        const containsDoctype = doctypes.includes(doctype);
        requests['doctype-banner'] = containsDoctype;
        const spaceBanner = doctypes.includes('#space-subject-banner');
        requests['space-banner'] = spaceBanner;
      }
      return requests;
    }

    const paddingLeft = parseInt($main.css("padding-left").replace('px', ''));
    const minWidth = paddingLeft > bannerWidthWithLabel ? bannerWidthWithLabel : bannerWidthWithoutLabel;
    const $sidebarContainer = AJS.$("#sidebar-container");
    if ($sidebarContainer.length) {
      const $propertiesElements = AJS.$(".projectdoc-document-element.properties");
      let doctype;
      if ($propertiesElements.length) {
        doctype = $propertiesElements.data("projectdoc-doctype");
      }

      const properties = fetchProperties(doctype);
      const bannerRequests = fetchBannerRequests(properties, doctype);

      if(!(bannerRequests['doctype-banner'] || bannerRequests['space-banner'])) {
        return;
      }

      const sizeCss = minWidth === bannerWidthWithLabel ? "large" : "small";
      const $banner = AJS.$("<div id=\"userscripts-vertical-banner\"></div>\n");
      $banner.addClass("projectdoc-" + sizeCss);

      let doctypeCss;
      if (doctype) {
        doctypeCss = encodeCss("projectdoc", doctype);
        $banner.addClass("projectdoc-banner-doctype");
        $banner.addClass(doctypeCss);
      }

      const spaceCategory = properties["subject"];
      let spaceCategoryCss;
      if (spaceCategory) {
        spaceCategoryCss = encodeCss("projectdoc", spaceCategory);
        $banner.addClass(spaceCategoryCss);
      }

      if (minWidth === bannerWidthWithLabel) {
        const $label = AJS.$("<div id=\"userscripts-vertical-banner-label\"></div>\n");
        const doctypeBannerRequested = doctype !== undefined && bannerRequests['doctype-banner'];
        if (doctypeBannerRequested) {
          $label.addClass(doctypeCss);
          const doctypeName = properties['doctypeName'];
          $label.text(doctypeName);
        } else if (bannerRequests['space-banner'] && spaceCategory) {
          $label.addClass(spaceCategoryCss);
          $label.text(spaceCategory);
        }
        $banner.append($label);
      }
      $sidebarContainer.append($banner);
    }
  }
});
