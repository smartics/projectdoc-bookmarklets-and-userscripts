/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = true;

  if (logToConsole) {
    AJS.log("[editor-boilerplates] Initializing boilerplates for the Confluence page editor ...");
  }

  if (!tinyMCE) {
    if (logToConsole) AJS.log("[editor-boilerplates] No editor found. Quitting ...");
    return;
  }
  AJS.log("[editor-boilerplates] Editor object found.");

  const insertIntoEditorAtSelection = function (boilerplateHtml) {
    const editor = tinyMCE.activeEditor;
    if (editor) {
      const newElementPlaceholderId = tinymce.DOM.uniqueId();
      editor.dom.add(editor.getBody(), 'span', {'id': newElementPlaceholderId}, '');

      const newElement = editor.dom.select('span#' + newElementPlaceholderId);
      if (newElement.length > 0) {
        editor.selection.select(newElement[0]);
        tinyMCE.activeEditor.selection.setContent(boilerplateHtml);
      }
    }
  };

  if (logToConsole) AJS.log("[editor-boilerplates] Editor found.");
  // insertIntoEditorAtSelection("<p>Hello <b>World</b>!</p>");

});
