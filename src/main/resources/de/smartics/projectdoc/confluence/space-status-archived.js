/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;

  if (logToConsole) {
    AJS.log("[space-status-archived] Checking status of space ...");
  }

  const spaceKey = AJS.Meta.get("space-key");
  const baseUrl = AJS.Meta.get('base-url');

  const serviceUrl = baseUrl + "/rest/api/space/?spaceKey=" + spaceKey + "&status=archived";
  const method = "GET";

  // Confluence 7
  AJS.$.ajax(serviceUrl, {
    async: true,
    type: method,
    contentType: 'application/json'
  }).success(function (data) {
    const isArchived = data.size === 1;
    if (logToConsole) AJS.log("[space-status-archived] The status of space '" + spaceKey + "' is '" + (isArchived ? "" : "not ") + "'archived'.");
    if (isArchived) {
      AJS.$("#main").css("background-color", "#FFCCCB");
    }
  }).error(function (jqXHR, textStatus) {
    if (jqXHR.status >= 200 && jqXHR.status < 300) {
      if (logToConsole) AJS.log("[space-status-archived] Successfully submitted to " + serviceUrl + "\n" + JSON.stringify(jqXHR));
    } else {
      if (logToConsole) AJS.log("[space-status-archived] Error submitting to " + serviceUrl + " " + jqXHR.status + " (" + textStatus + ")");
    }
  });

  // Confluence 9.1
  // const serviceUrl = baseUrl + "/rest/api/space/" + spaceKey;
  // const method = "GET";
  //
  // AJS.$.ajax(serviceUrl, {
  //   async: true,
  //   type: method,
  //   contentType: 'application/json'
  // }).success(function (data) {
  //   const status = data.status;
  //   if (logToConsole) AJS.log("[space-status-archived] The status of space '" + spaceKey + "' is '" + status + "'.");
  //   if(status === "archived") {
  //     AJS.$("#main").css("background-color", "#FFCCCB");
  //   }
  // }).error(function (jqXHR, textStatus) {
  //   if (jqXHR.status >= 200 && jqXHR.status < 300) {
  //     if (logToConsole) AJS.log("[space-status-archived] Successfully submitted to " + serviceUrl + "\n" + JSON.stringify(jqXHR));
  //   } else {
  //     if (logToConsole) AJS.log("[space-status-archived] Error submitting to " + serviceUrl + " " + jqXHR.status + " (" + textStatus + ")");
  //   }
  // });
});


