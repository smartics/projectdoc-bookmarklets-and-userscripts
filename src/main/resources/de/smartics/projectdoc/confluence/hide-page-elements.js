/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;

  if (logToConsole) {
    AJS.log("[hide-page-elements-anonymous] Checking ...");
  }

  const hide = function (...selectors) {
    AJS.$.each(selectors,
      function (index, selector) {
        if (logToConsole) AJS.log("[hide-page-elements] Hiding '" + selector + "'.");
        AJS.$(selector).hide();
      });
  };

  const hideParentOf = function (...selectors) {
    AJS.$.each(selectors,
      function (index, selector) {
        if (logToConsole) AJS.log("[hide-page-elements] Hiding parent of '" + selector + "'.");
        AJS.$(selector).parent().hide();
      });
  };

  const hideSidebar = function () {
    AJS.$('#nav-sidebar').hide();
    AJS.$('.ia-fixed-sidebar').hide();
    AJS.$('.ia-splitter-left').hide();
    AJS.$('#main').css("margin-left", "0");
  };

  const filterSearchControls = function () {
    const monitorCallback = function () {
      const $filterContainer = AJS.$("div[class^='SearchContainer_search-filter-list-container']");
      if ($filterContainer.length && ($filterContainer.is(':visible') || $filterContainer.is(':hidden'))) {
        $filterContainer.hide();
        const $searchTypeAheadContainer = AJS.$("div[class^='SearchContainer_search-typeahead-container']");
        $searchTypeAheadContainer.css('margin-left', '0');
        AJS.$("a[class^='SearchContainer_search-help-link']").hide();
      }
    };

    USERSCRIPT4C_SYNC.monitorElement(monitorCallback);
  };

  const ALL_PROFILE_PAGE_IDS = [
    '/users/viewmyprofile.action',
    '/users/viewuserprofile.action',
    '/inlinetasks/mytasks.action',
    '/users/viewmyfavourites.action',
    '/users/viewnotifications.action',
    'users/viewmydrafts.action',
    '/users/viewfollow.action',
    '/users/viewmysettings.action'];
  const hideProfileTabs = function (...urlIds) {
    if (ALL_PROFILE_PAGE_IDS.some(urlId => window.location.pathname.indexOf(urlId) !== -1)) {
      AJS.$('#breadcrumb-section').hide();
      const $navigation = AJS.$("div.user-profile>nav");
      if ($navigation) {
        $navigation.find("a").each(
          function () {
            const $anchor = $(this);
            if (urlIds.some(urlId => $anchor.attr('href') && $anchor.attr('href').indexOf(urlId) !== -1)) {
              $anchor.parent().hide();
            }
          });
      }
    }
  }

  const hideForAll = function () {
    hide('#comments-section');
  };

  const hideForAnonymous = function () {
    hideSidebar();
    hide('#likes-and-labels-container');

    hideParentOf(
      '#app-switcher',
      '#create-space-header',
      '#people-directory-link',
      '#tagcloud-link',
      '#help-menu-link',
      '#notifications-anchor',

      '#content-metadata-attachments',

      '#shareContentLink',
      '#watch-content-button',
      '#page-favourite',

      // page menu
      '#view-attachments-link',
      '#action-view-history-link',
      '#action-page-permissions-link',
      '#view-page-info-link',
      '#view-resolved-comments',
      '#view-in-hierarchy-link',
      '#action-view-source-link',
      '#action-source-editor-view-storage-link',
      '#action-view-storage-link',
      '#action-export-word-link',
      '#action-copy-page-link',
    );

    filterSearchControls();
  };

  const hideForConfluenceAdministrators = function () {
  };

  const hideForWebsiteUsers = function () {
    hideSidebar();
    hide(
      '#likes-and-labels-container',
      '#space-tools-menu-trigger');

    hideParentOf(
      '#app-switcher',
      '#people-directory-link',

      '#notifications-anchor',

      '#content-metadata-attachments',
      '#content-metadata-page-restrictions',

      '#watch-content-button',

      '#help-menu-link',

      // page menu
      '#view-attachments-link',
      '#action-view-history-link',
      '#action-page-permissions-link',
      '#view-page-info-link',
      '#view-resolved-comments',
      '#view-in-hierarchy-link',
      '#action-view-source-link',
      '#action-source-editor-view-storage-link',
      '#action-view-storage-link',
      '#action-export-word-link',
      // '#k15t-exp-word-export-dialog-web-item',

      // profile menu
      '#user-recently-worked-on',
      '#view-mytasks-link',
      '#user-watches-link',
      '#user-drafts-link',
      '#user-network-link',
      '#upm-requests-link'
    );

    hideProfileTabs(
      '/inlinetasks/mytasks.action',
      '/users/viewnotifications.action',
      'users/viewmydrafts.action',
      '/users/viewfollow.action')
  };

  const hideForConfluenceUsers = function () {
    hide('#likes-and-labels-container');
  };


  hideForAll();
  if (AJS.Meta.get('remote-user') == '') {
    hideForAnonymous();
  } else {

    const groups = USERSCRIPT4C_ACTIVE.getGroups();
    if (groups && groups.length) {

      if (USERSCRIPT4C_ACTIVE.contains('confluence-administrators', groups)) {
        hideForConfluenceAdministrators();
      } else if (USERSCRIPT4C_ACTIVE.contains('website-users', groups)) {
        hideForWebsiteUsers();
      } else if (USERSCRIPT4C_ACTIVE.contains('confluence-users', groups)) {
        hideForConfluenceUsers();
      }
    }
  }
});
