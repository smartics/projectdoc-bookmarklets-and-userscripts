/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;

  if (logToConsole) {
    AJS.log("[validate-page-projectdoc] Start validating page in a projectdoc environment ...");
  }

  const baseUrl = AJS.Meta.get('base-url');
  const delimIndex = baseUrl.lastIndexOf("/");
  const protocolIndex = baseUrl.indexOf(":") + 2;
  const serverUrl = delimIndex > protocolIndex ? baseUrl.substring(0, delimIndex) : baseUrl;
  if (logToConsole) AJS.log("[validate-page-projectdoc] Calculated Server URL: " + serverUrl);

  const i18n = PDBMLS.fetchI18n(baseUrl, ["spaceKey"]);
  const SPACE_KEY_NAME = i18n["spaceKey"];
  if (logToConsole) AJS.log("[validate-page-projectdoc] SPACE_KEY_NAME initialized: " + SPACE_KEY_NAME);

  const spaceKeyPattern = /\/display\/([^\/]+)\//i;
  const pageIdPattern = /pageId=(\d+)/;

  const invalidCss = {
    "background-color": "darkred",
    "color": "white",
    "font-weight": "bold"
  };
  const reportCors = false;
  const corsCss = {
    "background-color": "orange",
    "color": "black",
    "font-weight": "bold"
  };

  const spacePropertiesCache = {
    delegateSpaceClosure: [],
    searchSpaceClosure: [],
    allowedAccess: [],
    everythingAllowed: false,

    init: function (spaceKey) {
      const self = this;
      const serviceUrl = baseUrl + "/rest/projectdoc/1/space/" + spaceKey + "?property-filter=" + encodeURIComponent("projectdoc.delegate-space-closure|projectdoc.search-space-closure|projectdoc.reference-space-closure|search-space|reference-space");
      AJS.$.ajax({
        async: false,
        url: serviceUrl,
        dataType: 'json'
      }).success(function (data) {
        AJS.$.each(data.property, function (_index, property) {
          if (property.value) {
            if (logToConsole) AJS.log("Processing property '" + property.name + "' with value '" + property.value + "' ...");
            if ("projectdoc.delegate-space-closure" === property.name) {
              self.delegateSpaceClosure = property.value.split(/,\s?/);
            } else if ("projectdoc.search-space-closure" === property.name) {
              self.searchSpaceClosure = property.value.split(/,\s?/);
            } else if ("projectdoc.reference-space-closure" === property.name) {
              self.allowedAccess = property.value.split(/,\s?/);
            } else if ("search-space" === property.name || "reference-space" === property.name) {
              if (!self.everythingAllowed) {
                self.everythingAllowed = true;
              }
            }
          }
        });
        if (logToConsole) AJS.log("Configuration: " + JSON.stringify(self));
      }).error(function (jqXHR, textStatus) {
        const httpStatus = jqXHR.status;
        AJS.log(AJS.format("[validate-page-projectdoc] ERROR: Cannot fetch space closure: {0} ({1}:{2}), therefore allowing everything.", serviceUrl, textStatus, httpStatus));
        self.everythingAllowed = true;
      });

      if (logToConsole) AJS.log("[validate-page-projectdoc] Initialized\n---> delegateSpaceClosure=" + self.delegateSpaceClosure + "\n---> searchSpaceClosure=" + self.searchSpaceClosure + "\n---> allowedAccess=" + self.allowedAccess);
    },

    isSpaceAllowed(spaceKey) {
      if (logToConsole) AJS.log("[validate-page-projectdoc] Checking if space " + spaceKey + " is allowed ...");
      if (spaceKey) {
        return this.everythingAllowed
          || this.delegateSpaceClosure.includes(spaceKey)
          || this.searchSpaceClosure.includes(spaceKey)
          || this.allowedAccess.includes(spaceKey);
      }
      return false;
    },

    isAllowed: function (url) {
      const self = this;
      if (self.everythingAllowed) {
        return true;
      }
      const result = url.match(spaceKeyPattern);
      let spaceKey;
      if (!(url.startsWith("/") || url.startsWith(baseUrl))) {
        return true;
      }
      if (result && result.length === 2) {
        spaceKey = result[1];
      } else {
        const result = url.match(pageIdPattern);
        if (result && result.length === 2) {
          const pageId = result[1];
          const serviceUrl = baseUrl + "/rest/projectdoc/1/document/" + pageId + "?expand=property&resource-mode=value&property-filter=" + encodeURIComponent(SPACE_KEY_NAME);
          // if (logToConsole) AJS.log("[validate-page-projectdoc] Call: " + serviceUrl);
          AJS.$.ajax({
            async: false,
            url: serviceUrl,
            dataType: 'json'
          }).success(function (data) {
            AJS.$.each(data.property, function (_index, property) {
              if (property && SPACE_KEY_NAME === property.name) {
                spaceKey = property.value;
              }
            });
          }).error(function (jqXHR, textStatus) {
            const httpStatus = jqXHR.status;
            AJS.log(AJS.format("[validate-page-projectdoc] ERROR: Cannot fetch space key: {0} ({1}:{2})", serviceUrl, textStatus, httpStatus));
          });
        } else {
          // Links to Confluence pages outside spaces are allowed.
          return true;
        }
      }
      // AJS.log(AJS.format("[validate-page-projectdoc] Extracted space key: {0}", spaceKey));
      return self.isSpaceAllowed(spaceKey);
    }
  };

  if (logToConsole) AJS.log("[validate-page-projectdoc] Start validating page ...");

  const $content = AJS.$("#content");
  if (!$content.length || !$content.hasClass("page view")) {
    if (logToConsole) AJS.log("[validate-page-projectdoc] Not visiting a page. Quitting.");
    return;
  }

  const checkUrl = function (counter, linkTracker, url, $linkElement, label) {
    if (logToConsole) AJS.log("[validate-page-projectdoc] Starting call to check " + url + " ...");
    AJS.$.ajax({
      beforeSend: function (jqXHR, settings) {
        settings.url = url;
      },
      type: "HEAD",
      async: true,
      redirect: 'follow',
      url: url,
    }).success(function () {
      if (logToConsole) AJS.log(AJS.format("[validate-page-projectdoc] Valid URL: {0}", url));
      linkTracker.linksChecked++;

      if (!spacePropertiesCache.isAllowed(url)) {
        const ref = markElement(counter, $linkElement, invalidCss);
        const invalidUrl = {
          url: url,
          label: label,
          status: 400,
          ref: ref
        };
        linkTracker.invalidUrls.push(invalidUrl);
        renderReport('Interims Page Validation Report');
      }
    }).error(function (jqXHR, textStatus) {
      const httpStatus = jqXHR.status;
      if (logToConsole) AJS.log(AJS.format("[validate-page-projectdoc] Invalid URL: {0} ({1}:{2})", url, textStatus, httpStatus));

      if (httpStatus === 0) {
        if (logToConsole) AJS.log("[validate-page-projectdoc] CORS issue detected!");
      }
      if (httpStatus !== 0 || reportCors) {
        const ref = markElement(counter, $linkElement, httpStatus === 0 ? corsCss : invalidCss);
        const invalidUrl = {
          url: url,
          label: label,
          status: httpStatus,
          ref: ref
        };
        linkTracker.invalidUrls.push(invalidUrl);
        renderReport('Interims Page Validation Report');
      }
      linkTracker.linksChecked++;
    });
  };

  const findMessageContainer = function () {
    let $messageContainer = AJS.$("#messageContainer");
    if ($messageContainer.length) {
      const $li = AJS.$("<li></li>");
      $messageContainer.append($li);
      return $li;
    }

    $messageContainer = AJS.$("#action-messages");
    if ($messageContainer.length) {
      return $messageContainer;
    }

    $messageContainer = AJS.$("#full-height-container");
    return $messageContainer;
  };

  const markElement = function (counter, $element, css) {
    $element.css(css);

    let ref = $element.attr("id");
    if (!ref) {
      ref = "validate-page-" + counter;
      $element.attr("id", ref);
    }
    return ref;
  };

  const anchorsInContent = AJS.$("#main-content").find("a");
  const linkTracker = {
    linksProcessed: 0,
    processedUrls: [],
    invalidUrls: [],
    linksChecked: 0
  }
  spacePropertiesCache.init(AJS.Meta.get('space-key'));
  AJS.$.each(anchorsInContent, function () {
    linkTracker.linksProcessed++;
    const $currentAnchor = AJS.$(this);
    if (!$currentAnchor.length) AJS.log("[validate-page-projectdoc] Cannot access current anchor!!!!!!!!");
    const url = AJS.$.trim($currentAnchor.attr("href"));
    const label = $currentAnchor.text();

    linkTracker.processedUrls.push(url);

    if ($currentAnchor.hasClass("createlink")) {
      const invalidUrl = {
        url: url,
        label: label,
        status: "create link"
      };
      linkTracker.invalidUrls.push(invalidUrl);
      linkTracker.linksChecked++;
    } else {
      if (url) {
        if (logToConsole) AJS.log("[validate-page-projectdoc] Checking " + url + " ...");

        if (url.startsWith("#")) {
          if (url.length > 1) {
            const $element = AJS.$(url);
            if (!$element.length) {
              const invalidUrl = {
                url: url,
                label: label,
                status: "invalid-in-page-reference"
              };
              linkTracker.invalidUrls.push(invalidUrl);
              markElement(linkCounter, $currentAnchor);
            }
          }
          linkTracker.linksChecked++;
        } else if (url.startsWith("mailto:")) {
          linkTracker.linksChecked++;
        } else {
          let requestUrl;
          if (url.startsWith("/")) {
            requestUrl = serverUrl + url;
            checkUrl(linkTracker.linksProcessed, linkTracker, requestUrl, $currentAnchor, label);
          } else {
            requestUrl = url;
            checkUrl(linkTracker.linksProcessed, linkTracker, requestUrl, $currentAnchor, label);
            // if (logToConsole) AJS.log("Skip accessing remote URLs (CORS) ...");
          }
        }
      } else {
        linkTracker.linksChecked++;
      }
    }
  });

  const renderReport = function (title) {
    if (linkTracker.invalidUrls.length > 0) {
      if (logToConsole) AJS.log(AJS.format("[validate-page-projectdoc] Page found {0} issues according to configured checks!"), linkTracker.invalidUrls.length);
      const $messageContainer = findMessageContainer();
      if ($messageContainer.length) {
        const $message = AJS.$('<div id="userscript-page-report" class="aui-message aui-message-error">\n' +
          '<p class="title">\n' +
          '  <strong>' + title + '</strong>\n' +
          '</p>\n' +
          '<p>The following issues have been encountered.</p>\n' +
          '<h3>Invalid URLs</h3>\n' +
          '<p>The page contains ' + linkTracker.invalidUrls.length + ' URLs with issues.</p>\n' +
          '<ol id="invalid-urls"></ol>\n' +
          '</div>');

        const $ol = AJS.$($message).find("#invalid-urls");
        if ($ol.length) {
          AJS.$.each(linkTracker.invalidUrls, function (index, invalidUrl) {
            if (logToConsole) AJS.log("[validate-page-projectdoc] Processing invalid URL: " + JSON.stringify(invalidUrl));
            const $li = AJS.$('<li></li>');
            const $reportLine = AJS.$("<div>" + invalidUrl.url + " (<a href='#" + invalidUrl.ref + "'>" + invalidUrl.label + "</a>): " + invalidUrl.status + "</div>");
            $li.append($reportLine);
            $ol.append($li);
          });
          const $oldMessage = $messageContainer.find("#userscript-page-report");
          if ($oldMessage) {
            $oldMessage.remove();
          }
          $messageContainer.append($message);
        } else {
          AJS.log(AJS.format("[validate-page-projectdoc] Failed to locate own list by ID #invalid-urls! Skipping report ..."));
        }
      } else {
        AJS.log(AJS.format("[validate-page-projectdoc] Failed to locate message container on page with ID #messageContainer! Skipping report ..."));
      }
    } else {
      if (logToConsole) AJS.log("[validate-page-projectdoc] Page valid according to configured checks!");
    }
  };

  let intervalsCount = 0;
  let intervalId = setInterval(
    function () {
      intervalsCount++;
      if (logToConsole) AJS.log(intervalsCount + ". interval: Status of checks: checked:" + linkTracker.linksChecked + " vs. processed:" + linkTracker.linksProcessed);
      if (linkTracker.linksChecked >= linkTracker.linksProcessed || intervalsCount > 30) {
        if (logToConsole) AJS.log("Finished with checks, rendering report ...");
        if (linkTracker.linksChecked < linkTracker.linksProcessed) {
          let consoleReport = "List of processed links (checked:" + linkTracker.linksChecked + " vs. processed:" + linkTracker.linksProcessed + ")\n";
          AJS.$.each(linkTracker.processedUrls, function (index, processedUrl) {
            consoleReport += "  " + index + ". " + processedUrl + "\n";
          });
          AJS.log(consoleReport);
        }

        renderReport('Final Page Validation Report');
        clearInterval(intervalId);
        if (logToConsole) AJS.log("Report rendered.");
      }
    }, 1500);
});
