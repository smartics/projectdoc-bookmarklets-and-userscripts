/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = true;

  if (logToConsole) {
    AJS.log("[projectdoc-document-peek] Start peeking ...");
  }

  const baseUrl = AJS.Meta.get('base-url');
  const pageIdPattern = /pageId=(\d+)/;
  const spaceKeyTitlePattern = /([^\/]+)\/([^\/]+)$/;
  const i18n = PDBMLS.fetchI18n(baseUrl, ["doctype", "title", "spaceKey", "name", "shortDescription", "tinyUrl"]);
  const tinyUrlNamePlain = i18n["tinyUrl"] + '\u00a7';

  const signalError = function (message) {
    AJS.flag({
      type: 'error',
      close: 'auto',
      body: message
    });
  }

  AJS.$("#main-content").find("a").mouseenter(function (e) {
    if (e.shiftKey || e.ctrlKey) {
      const $link = AJS.$(this);
      let pageId = $link.data('projectdoc-id');
      if (!pageId) {
        const url = $link.attr('href');
        const result = url.match(pageIdPattern);
        if (result && result.length === 2) {
          pageId = result[1];
        } else {
          const result = url.match(spaceKeyTitlePattern);
          if (result.length == 3) {
            // const spaceKey = decodeURIComponent(result[1]);
            // const title = decodeURIComponent(result[2]);
            const encodedSpaceKey = result[1];
            const encodedTitle = result[2];

            const serviceUrl = "/rest/api/content?spaceKey=" + encodedSpaceKey + "&title=" + encodedTitle;
            AJS.$.ajax({
              async: false,
              url: serviceUrl,
              dataType: 'json'
            }).success(function (data) {
              if (data.results.length === 1) {
                pageId = data.results[0].id;
              } else {
                AJS.log("[projectdoc-document-peek] WARN Found " + data.results.length + " results: " + JSON.stringify(data));
                signalError("Cannot determine page ID for URL with space key / title pattern: " + url);
                return;
              }
            }).error(function (jqXHR, textStatus) {
              const httpStatus = jqXHR.status;
              signalError("Cannot determine page ID for URL: " + url + " (" + httpStatus + ")");
              return;
            });
          } else {
            signalError("Cannot determine page ID for URL: " + url);
            return;
          }
        }
      }

      if (pageId) {
        const currentDocument = PDBMLS.fetchDocument(baseUrl, pageId, [i18n.spaceKey, i18n.title, i18n.name, i18n.doctype, tinyUrlNamePlain]);
        if (currentDocument) {
          const tinyUrl = currentDocument[tinyUrlNamePlain];
          const message = "<strong>" + currentDocument[i18n.spaceKey] + "</strong> (" + currentDocument[i18n.doctype] + ")<br>" + currentDocument[i18n.title] + "<br><strong><a href='" + tinyUrl + "' target='_blank'>Open</a></strong>";
          AJS.flag({
            type: 'info',
            close: 'auto',
            body: message
          });
        } else {
          AJS.log("[projectdoc-document-peek] WARN Document with page ID " + pageId + " is not a projectdoc document!");
        }
      }
    }
  });
});
