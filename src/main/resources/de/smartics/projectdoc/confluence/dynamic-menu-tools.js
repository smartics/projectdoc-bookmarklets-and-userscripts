/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = true;

  if (logToConsole) AJS.log("[dynamic-menu-tools] Starting Tool menu creation ...");

  const menuId = "tools";
  const sectionId = "userscripts";

  const $mainMenu = USERSCRIPT4C_MENU.createMenu(menuId, "Tools");
  USERSCRIPT4C_MENU.registerMenu("view.menu", $mainMenu, "inspect");
  // In case you need to append the menu to an element identified by a selector, use this:
  //  USERSCRIPT4C_MENU.registerBySelector($mainMenu, "#my-id");
  USERSCRIPT4C_MENU.addSection(menuId, {
    id: sectionId,
    label: "Userscripts",
    weight: 10
  });
  USERSCRIPT4C_MENU.addMenuItem(sectionId, {
    id: "userscript-admin-tool",
    label: "Admin Tool",
    weight: "100"
  }, function () {
    alert("Open Admin Tool ...")
  });
  USERSCRIPT4C_MENU.addMenuItem(sectionId, {
    id: "before",
    label: "Before",
    weight: "10"
  }, function () {
    alert("Before ...")
  });
  USERSCRIPT4C_MENU.addMenuItem(sectionId, {
    id: "after",
    label: "After",
    weight: "200"
  }, function () {
    alert("After ...")
  });
  USERSCRIPT4C_MENU.addMenuItem(sectionId, {
    id: "last",
    label: "Last",
    weight: "99999"
  }, function () {
    alert("Last ...")
  });
  USERSCRIPT4C_MENU.addMenuItem(sectionId, {
    id: "first",
    label: "First",
    weight: "0"
  }, function () {
    alert("First ...")
  });
});

