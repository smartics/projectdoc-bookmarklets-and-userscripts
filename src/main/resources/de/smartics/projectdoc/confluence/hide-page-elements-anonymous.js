/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;

  if (logToConsole) {
    AJS.log("[hide-page-elements-anonymous] Checking ...");
  }

  if (AJS.Meta.get('remote-user') == '') {
    if (logToConsole) {
      AJS.log("[hide-page-elements-anonymous] Hiding likes, labels, and comments ...");
    }
    AJS.$('#likes-section').hide();
    AJS.$('#labels-section').hide();
    AJS.$('#comments-section').hide();

    if (logToConsole) {
      AJS.log("[hide-page-elements-anonymous] Hiding people and tag cloud ...");
    }
    // AJS.$('#space-directory-link').hide()
    AJS.$('#people-directory-link').hide();
    AJS.$('#tagcloud-link').hide();
    // AJS.$('#create-page-button').hide();

    if (logToConsole) {
      AJS.log("[hide-page-elements-anonymous] Hiding tags, wiki, calendar, and quick links ...");
    }
    //  AJS.$('.acs-nav-item.profile').hide();
    AJS.$('.acs-nav-item.tags').hide();
    AJS.$('.acs-nav-item.wiki').hide();
    AJS.$('.acs-nav-item.space-calendar-sidebar-link').hide();
    AJS.$('.acs-nav-item.blog').hide();
    AJS.$('.quick-links-wrapper').hide();
    //  AJS.$('.parent-item').hide();

    if (logToConsole) {
      AJS.log("[hide-page-elements-anonymous] Hiding space tools ...");
    }
    AJS.$('.space-tools-section').hide();

    if (logToConsole) {
      AJS.log("[hide-page-elements-anonymous] Hiding page menu items ...");
    }
    // AJS.$('#action-menu-link').hide();
    AJS.$('#view-attachments-link').hide();
    AJS.$('#action-view-history-link').hide();
    AJS.$('#view-page-info-link').hide();
    AJS.$('#view-resolved-comments').hide();
    AJS.$('#view-in-hierarchy-link').hide();
    AJS.$('#action-view-source-link').hide();
    //AJS.$('#action-export-pdf-link').hide();
    //AJS.$('#action-export-word-link').hide();
    AJS.$('#action-copy-page-link').hide();

    if (logToConsole) {
      AJS.log("[hide-page-elements-anonymous] Hiding help menu items ...");
    }
    // AJS.$('#help-menu-link').hide();
    // AJS.$('#confluence-help-link').hide();
    // AJS.$('#keyboard-shortcuts-link').hide();
    // AJS.$('#feed-builder-link').hide();
    AJS.$('#whats-new-menu-link').hide();
    AJS.$('#gadget-directory-link').hide();
    // AJS.$('#confluence-about-link').hide();
  }
});
