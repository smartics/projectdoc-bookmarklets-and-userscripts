/*
 * Copyright 2019-2024 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

AJS.toInit(function () {
  const logToConsole = false;

  if (logToConsole) {
    AJS.log("[install-shortcuts] Starting to install shortcuts ...");
  }

  const logInstall = function(name, shortcut, url) {
    if (logToConsole) AJS.log(AJS.format("[install-shortcuts] Installing shortcut to {0} to {1}: {2}", name, shortcut, url));
  }

  const baseUrl = AJS.Meta.get('base-url');

  const restApiBrowserUrl = baseUrl + "/plugins/servlet/restbrowser#/";
  const restApiBrowserShortcut = "22";
  logInstall("REST API Browser", restApiBrowserShortcut, restApiBrowserUrl);
  AJS.whenIType(restApiBrowserShortcut).goTo(restApiBrowserUrl);

  const userscriptShortcut = "qq";
  logInstall("Userscript Admin Tool", userscriptShortcut, "JavaScript call");
  AJS.whenIType(userscriptShortcut).click("#userscripts-admin-tool");
});
