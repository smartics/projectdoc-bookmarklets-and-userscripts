# projectdoc Bookmarklets and Userscripts

## Overview

This project provides free [bookmarklets](https://www.smartics.eu/confluence/x/ToG3Bw) 
and [userscripts](https://www.smartics.eu/confluence/x/LADaBw) for the 
[projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).


## Fork me!
Feel free to fork this project to adjust the scripts.

The artifacts of the project are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [project's homepage](https://www.smartics.eu/confluence/x/ToG3Bw)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Community Resources](https://www.smartics.eu/confluence/x/Ooa3Bw)
